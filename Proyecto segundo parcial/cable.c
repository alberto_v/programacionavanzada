#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int findName(char name[], char names[][20], int size){
	int i;
	for(i=0; i<size; i++){
		if(strcmp(name, names[i])==0){
			return i;
		}
	}
	return -1;
}

int findNumber(int n, int *numbers, int size){
	int i;
	for(i=0; i<size; i++){
		if(n==numbers[i]){
			return 1;
		}
	}
	return 0;
}

int main(){
	double cableLength, value, sumCable=0.0;
	int numberOfPersons, numberOfConnections, i, j, k, pos=1;
	char names[100][20];


	scanf("%lf", &cableLength);
	scanf("%d", &numberOfPersons);

	for(i=0; i<numberOfPersons; i++){
        scanf("%s", names[i]);
    }
	
	scanf("%d", &numberOfConnections);

	double table[numberOfPersons][numberOfPersons];
	for(k=0; k<numberOfConnections; k++){
		char name1[20], name2[20];
		scanf("%s", name1);
		scanf("%s", name2);
		scanf("%lf", &value);
		i=findName(name1, names, numberOfPersons);
		j=findName(name2, names, numberOfPersons);
		table[i][j]=value;
		table[j][i]=value;
	}


	//PRISM ALGORITHM (MINUMUM SPANNING TREE)

	int markedRows[numberOfPersons];
	int markedColumns[numberOfPersons];

	for(i=1; i<numberOfPersons; i++){
		markedRows[i]=-1;
		markedColumns[i]=-1;
	}
	markedRows[0]=0;
	markedColumns[0]=0;

	for (k=1; k<numberOfPersons; k++) {
		double min=9999999;
		int x=-1;
		for (i=0; i<numberOfPersons; i++) {
			for (j=0; j<numberOfPersons; j++) {
				if(!findNumber(i, markedRows, numberOfPersons) && findNumber(j, markedColumns, numberOfPersons) && table[i][j]<min && table[i][j]>0.01){
					min=table[i][j];
					x=i;
				}
			}
		}
		sumCable+=min;
		markedRows[k]=x;
		markedColumns[k]=x;
	}


	if(sumCable>cableLength){
		printf("Not enough cable\n");
	}
	else{
		printf("Need %.1lf miles of cable\n", sumCable);
	}

	return 0;
}