long sumOfIntegers(int arr_size, int* arr) {
    int i;
    long sum=0;
    
    for(i=0; i<arr_size; i++){
        sum+=arr[i];    
    }
    return sum;
}