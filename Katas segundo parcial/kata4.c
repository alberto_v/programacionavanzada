int maxDifference(int a_size, int* a) {
    int maxDiff=-1;
    int min=a[0];
    int i;
    for(i = 1; i < a_size; i++){       
        if(a[i]-min>maxDiff){                             
            maxDiff=a[i]-min;
        }
        if(a[i]<min){
            min=a[i];  
        }
    }
    return maxDiff;
}