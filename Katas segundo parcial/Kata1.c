int countDuplicates(int numbers_size, int* numbers) {
    int num[1000]={0};
    int cont=0;
    int i;
    
    for(i=0; i<numbers_size; i++){
        if(num[numbers[i]]==0){
            num[numbers[i]]++;
        }
        else if(num[numbers[i]]==1){
            cont++;
            num[numbers[i]]=-1;
        }
    }
    return cont;
}