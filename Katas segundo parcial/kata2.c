int isArray_Balanced(int arr_size, int* arr) {
    int i=0;
    int j=arr_size-1;
    int sumI=0;
    int sumD=0;
    int flagI=1;
    int flagD=1;
    
    while(i<j){
        if(flagI){
            sumI+=arr[i];
        }
        if(flagD){
            sumD+=arr[j];
        }
        if(sumI<sumD){
            i++;
            flagD=0;
            flagI=1;
        }
        else if(sumI>sumD){
            j--;
            flagD=1;
            flagI=0;
        }
        else{
            flagD=1;
            flagI=1;
            i++;
            j--;
        }
    }
    return sumI==sumD;
}