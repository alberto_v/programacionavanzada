int KDifference(int a_size, int* a, int k) {    
    int i, j;
    int cont=0;
    
    for(i=0; i<a_size; i++){
        for(j=i+1; j<a_size; j++){
            if(abs(a[i]-a[j])==k){
                cont++;
            }
        }
    }
    return cont;
}