#include <stdio.h>

int main(){
	int tests, i, j, powerStrips, outlet, poweredAppliances, k;
	scanf("%d", &tests);
	int resultados[tests];

	for(i=0; i<tests; i++){
		scanf("%d", &powerStrips);
		poweredAppliances=0;
		for (j=0; j<powerStrips; j++){
			scanf("%d", &outlet);
			poweredAppliances+=outlet;
		}
		poweredAppliances+=-powerStrips+1;
		resultados[i]=poweredAppliances;
	}
	for(k=0; k<tests; k++){
		printf("%d\n", resultados[k]);
	}
	return 0;
}
