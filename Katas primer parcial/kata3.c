void mystery(int letter_size, char** letter) {
    int i, j;
    for(i=0; i<letter_size; i++) {
        int cont=0;
        for(j=0; j<strlen(letter[i])/2; j++){
            cont += abs((int)letter[i][j]-(int)letter[i][strlen(letter[i])-1-j]);
        }
        printf("%d\n", cont);
    }
}