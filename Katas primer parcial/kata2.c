char* isPangram(int strings_size, char** strings) {
    int i, j, k;
    char *res = "";
    
    for(i=0; i<strings_size; i++){
        int * letras=malloc(sizeof (int) * 26);
        for(j=0; j<strlen(strings[i]); j++){
            int value=(int)strings[i][j];
            if(value!=32){
                if(letras[value-97]==0){
                    letras[value-97]++;
                }
            }
        }
        
        for(k=0; k<26; k++){
            if(letras[k]==1){
                if(k==25){
                    size_t len = strlen(res);
                    char *res2 = malloc(len + 1 + 1 );
                    strcpy(res2, res);
                    res2[len] = '1';
                    res2[len + 1] = '\0';
                    res=res2;
                }
            }
            else{
                size_t len = strlen(res);
                char *res2 = malloc(len + 1 + 1 );
                strcpy(res2, res);
                res2[len] = '0';
                res2[len + 1] = '\0';
                res=res2;
                break;
            }
        }
    }
    
    return res;
}