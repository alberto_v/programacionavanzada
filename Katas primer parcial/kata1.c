int gemstones(int rocks_size, char** rocks) {
    int * letras=malloc(sizeof (int) * 26);
    
    int i, j;
    for(i=0; i<rocks_size; i++) {
        for(j=0; j<strlen(rocks[i]); j++){
            int value=(int)rocks[i][j];
            if(letras[value-97]==i){
                letras[value-97]++;
            }
        }
    }
    int cont=0;
    for(i=0; i<26; i++){
        if(letras[i]==rocks_size){
            cont++;
        }
    }
    return cont;
}