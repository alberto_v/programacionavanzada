#include "lsearch.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int testIntA(void);
int testCharA(void);
int testStringA(void);

int main(){
  //testIntA();
  //testCharA();
  testStringA();
  return 0;
}

int testIntA(){
  int a[] = {2, 3, 4, 5};
  if(lsearch(4, a, 4) > -1){
    printf("Número encontrado. \n" );
  }else{
    printf("Número no encontrado. \n" );
  }

  return 0;
}

int testCharA(){
  char c = 'z';
  char *st = strdup("Perez");
  char *r;

  r = (char*) lsearchgeneric((void*)&c, (void*)st, sizeof(char), strlen(st), cmpchar);

  if(r){
    printf("Elemento encontrado en la dirección: %p\n", r);
  }else{
    printf("Elemento no encontrado.\n");
  }

  return 0;
}

int testStringA(){
	//Es importante que este valor sea igual al número de palabras que contenga la variable "words"
	int NumberOfWords=5;

	//Creamos el arreglo de strings
	char ** words=(char **) malloc(NumberOfWords * sizeof(char*));
	words[0]=strdup("holaaaaaaa");
	words[1]=strdup("que");
	words[2]=strdup("hace");
	words[3]=strdup("alberto");
	words[4]=strdup("villagomez");

	//Este es el string a buscar
	char * key=strdup("ill");

	char * r;
	r=(char *)lsearchgenericString((void*)key, (void**)words, sizeof(char *), NumberOfWords*sizeof(char *));

	if(r){
    	printf("Elemento encontrado en la dirección: %p\n", r);
  	}
  	else{
    	printf("Elemento no encontrado.\n");
  	}
	return 0;
}
