int lsearch(int, int[], int);
void* lsearchgeneric(void*, void*, int, int, int (*cmpf)(void*, void*));
int cmpchar(void*, void*);

void* lsearchgenericString(void*, void**, int, int);