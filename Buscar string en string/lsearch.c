#include <stdio.h>
#include <string.h>

int lsearch(int elem, int array[], int size){
  int i;
  for(i = 0; i < size; i++){
    if(elem == array[i]){
      return i;
    }
  }

  return -1;
}

//El último parámetro de esta función es un apuntador a función.
void* lsearchgeneric(void* elem, void *array, int elemsize, int size, int (*cmpf)(void*, void*)){
  int i = 0;
  void* addres;
  for(i = 0; i < size; i++){
    addres = (char*) array + i * elemsize;
    if(cmpf(elem, addres)){
      return addres;
    }
  }
  return 0;
}

//Debe recibir genéricos para que cumpla el prototipo de la función genérica.
int cmpchar(void* a, void* b){
  char va = *(char* )a;
  char vb = *(char* )b;

  return va == vb;
}


//El método strstr() nos ahorra usar otro método para buscar un substring dentro de un string
void* lsearchgenericString(void * elem, void ** matriz, int elemsize, int size){
	int i;
	void* address;
	for (i = 0; i < size/elemsize; i++){
		address = (char*) matriz[0] + i * 32;
		if(strstr(matriz[i], elem)!=NULL){
			//Calculamos la direccion relativo al char dentro del string donde ocurrió el match
			//Lo sumamos a la direccion
			int DireccionEnString=strlen(matriz[i])-strlen(strstr( matriz[i], elem ));
			address=(char*)address+DireccionEnString;
			//printf("Direccion final %p\n", address);
			return address;
		}
	}
	return 0;
}