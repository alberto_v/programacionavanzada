/*
Solucion http://acm.tju.edu.cn/toj/showp1218.html
Programador: mpalomera
*/

#include <stdio.h>

int get_majacolumn(int);
int get_majarow(int);

int main(){
	int willnumber = 0;
	int majacolumn = 0, majarow =0;

	//Read input
	scanf("%d",&willnumber);
	while(willnumber >0){
		//Process
		majacolumn = get_majacolumn(willnumber);
		majarow =  get_majarow(willnumber);
		//Print result
		printf("%d,%d\n",majacolumn,majarow);
		//Read input
		scanf("%d",&willnumber);
	}
	return 0;
}

int get_majarow(int willnumber){
	int row = 0;
	int currentcelda = 1;
	int minrow = 0, maxrow = 0;
	int maxcapacity = 1;
	int i;

	if(willnumber == 1)
		return 0;

	while(currentcelda < willnumber){
		currentcelda += 1;
		minrow -= 1;
		maxrow += 1;
		maxcapacity += 1;
		row+=1;

		//Desplaza a la derecha 
		for(;row < maxrow; row++,currentcelda++){
			if(currentcelda == willnumber)
				return row;
		}

		//Llena los extremos de la derecha
		for(i = 0;i<maxcapacity;currentcelda++,i++){
			if(currentcelda == willnumber)
				return row;
		}

		//Desplaza izquierda
		for(row-=1;row > minrow ;row--,currentcelda++){
			if(currentcelda == willnumber)
				return row;
		}

		//Llenar los extremos de la izquierda
		for(i = 0;i<maxcapacity-1;currentcelda++,i++){
			if(currentcelda == willnumber)
				return row;
		}
	}
	return row;
}

int get_majacolumn(int willnumber){
	int column = 0;
	int currentcelda = 1;
	int mincolumn = 0, maxcolumn = 0;
	int maxcapacity = 1;
	int i;
	if (willnumber == 1)
		return  0;
	while(currentcelda < willnumber){
		currentcelda += 1;
		if (currentcelda == willnumber) return column;
		mincolumn -= 1;
		maxcolumn += 1;
		maxcapacity += 1;
		column -= 1;
		//desplaza izquierda
		for(currentcelda += 1;column > mincolumn ; column--,currentcelda++){
			if(currentcelda == willnumber)
				return column;
		}
		for(i = 0;i<maxcapacity;currentcelda++,i++){
			if(currentcelda == willnumber)
				return column;
		}
		for(column += 1;column < maxcolumn ; column++,currentcelda++){
			if(currentcelda == willnumber)
				return column;
		}
		for(i = 0;i<maxcapacity -1;currentcelda++,i++){
			if(currentcelda == willnumber)
				return column;
		}


	}
	return column;
}
