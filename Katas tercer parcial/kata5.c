/*
 * Complete the function below.
 * Please store the size of the string array to be returned in result_size pointer. For example,
 * char a[2][6] = {"hello", "world"};
 * *result_size = 2;
 * return a;
 * 
 */
char** ReduceFraction(int fractions_size, char** fractions, int* result_size) {
    int i;
    *result_size = fractions_size;
    char ** res = (char**)malloc(sizeof(char*) * fractions_size);
    
    for(i=0; i<fractions_size; i++){
        //Sacar números
        char * linea = fractions[i];
        int a = atoi(linea);
        while(linea[0] != '/'){
            linea++;
        }
        linea++;
        int b = atoi(linea);
        
        //Calucular mcd
        int g = mcd(a, b);
        
        //Convertir a string y guardar
        char str[5];
        char str2[5];
        char frac[10] = {0};
        sprintf(str, "%d", a/g);
        sprintf(str2, "%d", b/g);
        
        int j, k, temp=0;
        
        for(j=0; j<strlen(str); j++){
            frac[j] = str[j];
            temp = j;
        }
        temp++;
        frac[temp] = '/';
        temp++;
        for(k=0; k<strlen(str2); k++){
            frac[temp] = str2[k];
            temp++;
        }
        
        res[i] = strdup(frac);
    }
    return res;
}

int mcd(int a, int b){
    if (a==0){
        return b;
    }
    return mcd(b%a, a);
}