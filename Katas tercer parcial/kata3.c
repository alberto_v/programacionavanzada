#include <stdio.h>
int main() {
    int i, k, n, c, m, dulces, residuo;
    scanf("%d", &k);
    
    for(i=0; i<k; i++){
        scanf("%d %d %d", &n, &c, &m);
        dulces = residuo = n/c;
        
        while(residuo >= m){
            dulces += residuo/m;
            residuo = residuo/m + residuo%m;
        }
        
        printf("%d\n", dulces);
    }
    return 0;
}