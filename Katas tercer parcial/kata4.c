/*
 * Complete the function below.
 */
int* countTwos(int arr_size, int* arr, int* result_size) {
    *result_size = arr_size;
    int * num = malloc(sizeof(int) * arr_size);
    int i;
    for(i=0; i<arr_size; i++){
        num[i] = isPowerTwo(arr[i]);
    }
    return num;
}

int isPowerTwo(int n){
    int movement = (int)(log(n)/log(2));
    return ( (n != 0 ) && ( (n >> movement) << movement ) == n );
}