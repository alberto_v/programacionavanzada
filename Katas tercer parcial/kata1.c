/*
 * Complete the function below.
 */
void countX(int arr_size, char** arr) {
    int i;
    long int minA, minB, numA, numB;
    
    for(i=0; i<arr_size; i++){
        char * linea = arr[i];
        numA = atoi(linea);
        
        while(linea[0] != ' '){
            linea++;
        }
        linea++;
        numB = atoi(linea);
        
        if(i==0){
            minA = numA;
            minB = numB;
        }
        
        if(numA<minA){
            minA = numA;
        }
        if(numB<minB){
            minB = numB;
        }
    }
    
    printf("%ld", minA*minB);
}