#include <stdio.h>
int main() {
    int k, i, j, * prev, * next;
    scanf("%d", &k);
    
    printf("1\n");
    
    for(i=2; i<=k; i++){
        free(next);
        next = malloc(sizeof(int) * i);
        for(j=0; j<i; j++){
            if(j == 0 || j == i-1){
                next[j] = 1;
            }
            else{
                printf("");
                next[j] = prev[j-1] + prev[j];
            }
        }
        printArray(next, i);
        free(prev);
        prev = malloc(sizeof(int) * (i+1));
        copyContents(prev, next, i);
    }
    return 0;
}
                   
void printArray(int * arr, int size){
    int i;
    for(i=0; i<size; i++){
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void copyContents(int * prev, int * next, int size){
    int i;
    for(i=0; i<size; i++){
        prev[i] = next[i];
    }
}